This is a PHP script that creates animated map sprites of Fire Emblem Fates head and class graphics. You can easily add your own ressources to quickly create tons of sprites.

![Nino Dark Mage Sprite](https://i.imgur.com/R4OwD7q.gif)
![Nino Pegasus Knight Sprite](https://i.imgur.com/CRZNiJC.gif)

## How to use:
- Upload the folder to a web server with PHP support or set up a local one
- Create a "output" folder
- Visit the location of the script in your webbrowser and add "?character=NAME" where name matches the characters name. (For example: http://localhost/fates-spriter/fe14_sprite_generator.php?character=Azura)

This was just a little side project to test PHPs image manipulation possiblities. Feel free to use it as you want.
The script has been created before PHP8 so if it shouldn't work on PHP8 you need a server with PHP7.
