<?php

// FE14 Sprite Generator made by HeartHero
// Feel free to use this tool but I would appreciate if you credit me

set_time_limit (0);

if (!isset($_GET["character"])) {
	exit;
}

$classes = glob("classes/*.png");
$chars = glob("characters/*.png"); // For complete-Loop
$char = $_GET["character"];

class GifCreator
{
    /**
     * @var string The gif string source (old: this->GIF)
     */
    private $gif;
    
    /**
     * @var string Encoder version (old: this->VER)
     */
	private $version;
    
    /**
     * @var boolean Check the image is build or not (old: this->IMG)
     */
    private $imgBuilt;

    /**
     * @var array Frames string sources (old: this->BUF)
     */
	private $frameSources;
    
    /**
     * @var integer Gif loop (old: this->LOP)
     */
	private $loop;
    
    /**
     * @var integer Gif dis (old: this->DIS)
     */
	private $dis;
    
    /**
     * @var integer Gif color (old: this->COL)
     */
	private $colour;
    
    /**
     * @var array (old: this->ERR)
     */
	private $errors;
 
    // Methods
    // ===================================================================================
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->reset();
        
        // Static data
        $this->version = 'GifCreator: Under development';
        $this->errors = array(
            'ERR00' => 'Does not supported function for only one image.',
    		'ERR01' => 'Source is not a GIF image.',
    		'ERR02' => 'You have to give resource image variables, image URL or image binary sources in $frames array.',
    		'ERR03' => 'Does not make animation from animated GIF source.',
        );
    }

	/**
     * Create the GIF string (old: GIFEncoder)
     * 
     * @param array $frames An array of frame: can be file paths, resource image variables, binary sources or image URLs
     * @param array $durations An array containing the duration of each frame
     * @param integer $loop Number of GIF loops before stopping animation (Set 0 to get an infinite loop)
     * 
     * @return string The GIF string source
     */
	public function create($frames = array(), $durations = array(), $loop = 0)
    {
		if (!is_array($frames) && !is_array($GIF_tim)) {
            
            throw new \Exception($this->version.': '.$this->errors['ERR00']);
		}
        
		$this->loop = ($loop > -1) ? $loop : 0;
		$this->dis = 2;
        
		for ($i = 0; $i < count($frames); $i++) {
		  
			if (is_resource($frames[$i])) { // Resource var
                
                $resourceImg = $frames[$i];
                
                ob_start();
                imagegif($frames[$i]);
                $this->frameSources[] = ob_get_contents();
                ob_end_clean();
                
            } elseif (is_string($frames[$i])) { // File path or URL or Binary source code
			     
                if (file_exists($frames[$i]) || filter_var($frames[$i], FILTER_VALIDATE_URL)) { // File path
                    
                    $frames[$i] = file_get_contents($frames[$i]);                    
                }
                
                $resourceImg = imagecreatefromstring($frames[$i]);
                
                ob_start();
                imagegif($resourceImg);
                $this->frameSources[] = ob_get_contents();
                ob_end_clean();
                 
			} else { // Fail
                
                throw new \Exception($this->version.': '.$this->errors['ERR02'].' ('.$mode.')');
			}
            
            if ($i == 0) {
                
                $colour = imagecolortransparent($resourceImg);
            }
            
			if (substr($this->frameSources[$i], 0, 6) != 'GIF87a' && substr($this->frameSources[$i], 0, 6) != 'GIF89a') {
			 
                throw new \Exception($this->version.': '.$i.' '.$this->errors['ERR01']);
			}
            
			for ($j = (13 + 3 * (2 << (ord($this->frameSources[$i] { 10 }) & 0x07))), $k = TRUE; $k; $j++) {
			 
				switch ($this->frameSources[$i] { $j }) {
				    
					case '!':
                    
						if ((substr($this->frameSources[$i], ($j + 3), 8)) == 'NETSCAPE') {
                            
                            throw new \Exception($this->version.': '.$this->errors['ERR03'].' ('.($i + 1).' source).');
						}
                        
					break;
                        
					case ';':
                    
						$k = false;
					break;
				}
			}
            
            unset($resourceImg);
		}
		
        if (isset($colour)) {
            
            $this->colour = $colour;
                                    
        } else {
            
            $red = $green = $blue = 0;
            $this->colour = ($red > -1 && $green > -1 && $blue > -1) ? ($red | ($green << 8) | ($blue << 16)) : -1;
        }
        
		$this->gifAddHeader();
        
		for ($i = 0; $i < count($this->frameSources); $i++) {
		  
			$this->addGifFrames($i, $durations[$i]);
		}
        
		$this->gifAddFooter();
        
        return $this->gif;
	}
    
    // Internals
    // ===================================================================================
    
	/**
     * Add the header gif string in its source (old: GIFAddHeader)
     */
	public function gifAddHeader()
    {
		$cmap = 0;

		if (ord($this->frameSources[0] { 10 }) & 0x80) {
		  
			$cmap = 3 * (2 << (ord($this->frameSources[0] { 10 }) & 0x07));

			$this->gif .= substr($this->frameSources[0], 6, 7);
			$this->gif .= substr($this->frameSources[0], 13, $cmap);
			$this->gif .= "!\377\13NETSCAPE2.0\3\1".$this->encodeAsciiToChar($this->loop)."\0";
		}
	}
    
	/**
     * Add the frame sources to the GIF string (old: GIFAddFrames)
     * 
     * @param integer $i
     * @param integer $d
     */
	public function addGifFrames($i, $d)
    {
		$Locals_str = 13 + 3 * (2 << (ord($this->frameSources[ $i ] { 10 }) & 0x07));

		$Locals_end = strlen($this->frameSources[$i]) - $Locals_str - 1;
		$Locals_tmp = substr($this->frameSources[$i], $Locals_str, $Locals_end);

		$Global_len = 2 << (ord($this->frameSources[0 ] { 10 }) & 0x07);
		$Locals_len = 2 << (ord($this->frameSources[$i] { 10 }) & 0x07);

		$Global_rgb = substr($this->frameSources[0], 13, 3 * (2 << (ord($this->frameSources[0] { 10 }) & 0x07)));
		$Locals_rgb = substr($this->frameSources[$i], 13, 3 * (2 << (ord($this->frameSources[$i] { 10 }) & 0x07)));

		$Locals_ext = $Locals_ext = "!\xF9\x04" . chr ((( $this->dis << 2 )) | 1 + 0 ) .chr(($d >> 0 ) & 0xFF).chr(($d >> 8) & 0xFF)."\x0\x0";
		

		if ($this->colour > -1 && ord($this->frameSources[$i] { 10 }) & 0x80) {
		  
			for ($j = 0; $j < (2 << (ord($this->frameSources[$i] { 10 } ) & 0x07)); $j++) {
			 
				if (ord($Locals_rgb { 3 * $j + 0 }) == (($this->colour >> 16) & 0xFF) &&
					ord($Locals_rgb { 3 * $j + 1 }) == (($this->colour >> 8) & 0xFF) &&
					ord($Locals_rgb { 3 * $j + 2 }) == (($this->colour >> 0) & 0xFF)
				) {
					$Locals_ext = "!\xF9\x04".chr(($this->dis << 2) + 1).chr(($d >> 0) & 0xFF).chr(($d >> 8) & 0xFF).chr($j)."\x0";
					break;
				}
			}
		}
        
		switch ($Locals_tmp { 0 }) {
		  
			case '!':
            
				$Locals_img = substr($Locals_tmp, 8, 10);
				$Locals_tmp = substr($Locals_tmp, 18, strlen($Locals_tmp) - 18);
                                
			break;
                
			case ',':
            
				$Locals_img = substr($Locals_tmp, 0, 10);
				$Locals_tmp = substr($Locals_tmp, 10, strlen($Locals_tmp) - 10);
                                
			break;
		}
        
		if (ord($this->frameSources[$i] { 10 }) & 0x80 && $this->imgBuilt) {
		  
			if ($Global_len == $Locals_len) {
			 
				if ($this->gifBlockCompare($Global_rgb, $Locals_rgb, $Global_len)) {
				    
					$this->gif .= $Locals_ext.$Locals_img.$Locals_tmp;
                    
				} else {
				    
					$byte = ord($Locals_img { 9 });
					$byte |= 0x80;
					$byte &= 0xF8;
					$byte |= (ord($this->frameSources[0] { 10 }) & 0x07);
					$Locals_img { 9 } = chr($byte);
					$this->gif .= $Locals_ext.$Locals_img.$Locals_rgb.$Locals_tmp;
				}
                
			} else {
			 
				$byte = ord($Locals_img { 9 });
				$byte |= 0x80;
				$byte &= 0xF8;
				$byte |= (ord($this->frameSources[$i] { 10 }) & 0x07);
				$Locals_img { 9 } = chr($byte);
				$this->gif .= $Locals_ext.$Locals_img.$Locals_rgb.$Locals_tmp;
			}
            
		} else {
		  
			$this->gif .= $Locals_ext.$Locals_img.$Locals_tmp;
		}
        
		$this->imgBuilt = true;
	}
    
	/**
     * Add the gif string footer char (old: GIFAddFooter)
     */
	public function gifAddFooter()
    {
		$this->gif .= ';';
	}
    
	/**
     * Compare two block and return the version (old: GIFBlockCompare)
     * 
     * @param string $globalBlock
     * @param string $localBlock
     * @param integer $length
     * 
     * @return integer
	 */
	public function gifBlockCompare($globalBlock, $localBlock, $length)
    {
		for ($i = 0; $i < $length; $i++) {
		  
			if ($globalBlock { 3 * $i + 0 } != $localBlock { 3 * $i + 0 } ||
				$globalBlock { 3 * $i + 1 } != $localBlock { 3 * $i + 1 } ||
				$globalBlock { 3 * $i + 2 } != $localBlock { 3 * $i + 2 }) {
				
                return 0;
			}
		}

		return 1;
	}
    
	/**
     * Encode an ASCII char into a string char (old: GIFWord)
     * 
     * $param integer $char ASCII char
     * 
     * @return string
	 */
	public function encodeAsciiToChar($char)
    {
		return (chr($char & 0xFF).chr(($char >> 8) & 0xFF));
	}
    
    /**
     * Reset and clean the current object
     */
    public function reset()
    {
        $this->frameSources;
        $this->gif = 'GIF89a'; // the GIF header
        $this->imgBuilt = false;
        $this->loop = 0;
        $this->dis = 2;
        $this->colour = -1;
    }
    
    // Getter / Setter
    // ===================================================================================
    
	/**
     * Get the final GIF image string (old: GetAnimation)
     * 
     * @return string
	 */
	public function getGif()
    {
		return $this->gif;
	}
}

function seperate_layer($filename,$layer,$size,$type) {

	$resource = imagecreatefrompng($filename);
	imagesavealpha($resource, true);
	
	$height = ($type == "class") ? 32 : 64;

	// Crop & arrange data

	$transparent = imagecolorallocatealpha($resource,255,255,255,127);

	$main = imagecreatetruecolor(128, $height);
	$mask = imagecreatetruecolor(128, $height);
	imagefill($main, 0, 0, $transparent);

	imagesavealpha($main, true);
	imagealphablending($main, false);
	imagesavealpha($mask, true);

	imagecopy($main,$resource,0,0,0,0,128,$height);
	imagecopy($mask,$resource,0,0,128,0,128,$height);

	// Differential-Deletion

	$color = ($layer == "mid") ? imagecolorallocate($mask,136,136,136) : imagecolorallocate($mask,102,102,102);
	// Colors for multiply (Children + Corrin)
	$white1 = imagecolorallocate($mask,255,255,255);
	$white2 = imagecolorallocate($mask,238,238,238);
	
	for ($y=0;$y<imagesy($mask);$y++) {
		
		for ($x=0;$x<imagesx($mask);$x++) {
			if (imagecolorat($mask,$x,$y) !== $color ) {
				if (imagecolorat($mask,$x,$y) == $white1 || (imagecolorat($mask,$x,$y) == $white2 && $layer !== "mid") ) {
					// Multiplication for dynamic hair color
					$rgb = imagecolorat($main, $x, $y);
					$TabColors=imagecolorsforindex ( $main , $rgb );
					$hex = (isset($_GET['hex'])) ? '#'.$_GET['hex'] : '#ffffff';
					list($r, $g, $b) = sscanf($hex, "#%02x%02x%02x");

					$color_r=floor($TabColors['red']*$r/255);
					$color_g=floor($TabColors['green']*$g/255);
					$color_b=floor($TabColors['blue']*$b/255);

					$newcol = imagecolorallocate($main, $color_r,$color_g,$color_b);
					imagesetpixel($main, $x, $y, $newcol);
					
				}else{
					// Delete invalid areas for current layer
					imagesetpixel ($main,$x,$y,$transparent);
				}
			}
		}
	}
	imagealphablending($main, true);
	
	$sprites = [];
	
	// Split sheet in layers
	
	for ($i=0;$i<4;$i++) {
		
		if ($size == 16) {
			$output = imagecreatetruecolor(16, 16);
		}else{
			$output = imagecreatetruecolor(32, 32);
		}
		
		imagesavealpha($output, true);
		imagefill($output, 0, 0, $transparent);
		
		if ($size == 16) {
			imagecopy($output,$main,0,0,($i*16),32,16,16);
		}else{
			imagecopy($output,$main,0,0,($i*32),0,32,32);
		}		
		$sprites[] = $output;
	}
	// Return layers as array
	return $sprites;
	
}

// Create animations and fill directory with files

$html_data = "";

// For complete loop of characters (Takes a lot of time!)
//for ($c_i=0;$c_i<count($chars);$c_i++) {
//$char = basename($chars[$c_i], ".png");

//mkdir("output/".$char, 0700);

for ($anim=0;$anim<count($classes);$anim++) {

	$class_back = seperate_layer($classes[$anim],"back",32,"class");
	$class_mid = seperate_layer($classes[$anim],"mid",32,"class");
	$class = basename($classes[$anim], ".png");

	// Set data for head-placement (Class-dependent)

	$head_size = 32;

	switch ($class) {
		case "SKY_KNIGHT":
		case "FALCON_KNIGHT":
		case "DARK_FLIER_F":
		case "DARK_FLIER_M":
			$headx = [10,10,9,9];
			$heady = [-1,0,0,-1];	
			$head_size = 16;
		break;
		case "KINSHI_KNIGHT":
		case "WYVERN_RIDER":
		case "WYVERN_LORD":
		case "MALIG_KNIGHT":
			$headx = [6,6,6,6];
			$heady = [2,1,1,0];
			$head_size = 16;
		break;
		case "CAVALIER_F":
		case "CAVALIER_M":
			$headx = [10,10,10,10];
			$heady = [2,2,2,2];	
			$head_size = 16;			
		break;
		case "GREAT_KNIGHT":
		case "BOW_KNIGHT":
		case "PALADIN":
		case "MECHANIST":
		case "DARK_KNIGHT":
		case "TROUBADOUR_F":
		case "TROUBADOUR_M":
			$headx = [9,9,9,9];
			$heady = [0,0,0,0];	
			$head_size = 16;			
		break;
		case "STRATEGIST":
			$headx = [9,9,9,9];
			$heady = [0,0,0,1];		
			$head_size = 16;			
		break;
		case "BALLISTICIAN":
			$headx = [8,8,8,8];
			$heady = [0,0,0,1];		
			$head_size = 16;			
		break;
		case "MAID":
		case "BUTLER":
		case "DARK_MAGE_F":
		case "DARK_MAGE_M":
		case "SORCERER_F":
		case "SORCERER_M":
		case "SHRINE_MAIDEN":
		case "PRIESTESS":
		case "GREAT_MASTER":
			$headx = [0,0,0,0];
			$heady = [-3,-3,-3,-3];
		break;
		case "MONK":
			$headx = [0,0,0,0];
			$heady = [-2,-2,-2,-2];
		break;
		case "MERCENARY_F":
			$headx = [0,0,0,0];
			$heady = [-1,0,1,0];
		break;
		case "MERCENARY_M":
			$headx = [0,0,0,0];
			$heady = [-2,-1,0,-1];
		break;
		case "HERO_F":
			$headx = [1,1,1,1];
			$heady = [-2,-1,-1,0];
		break;
		case "HERO_M":
			$headx = [0,0,0,0];
			$heady = [-2,-2,-1,-1];
		break;
		case "ADVENTURER_F":
		case "ADVENTURER_M":
		case "DIVINER_F":
		case "DIVINER_M":
			$headx = [0,0,0,0];
			$heady = [-2,-2,-2,-2];
		break;
		case "FIGHTER_F":
		case "FIGHTER_M":
			$headx = [1,1,1,1];
			$heady = [-1,-1,-2,-1];
		break;
		case "BERSERKER_F":
		case "BERSERKER_M":
			$headx = [0,0,0,0];
			$heady = [-2,-2,-3,-2];
		break;
		case "SAMURAI_F":
			$headx = [1,1,1,1];
			$heady = [-2,-2,-1,-2];
		break;
		case "SAMURAI_M":
			$headx = [2,2,2,2];
			$heady = [-1,-1,-1,0];
		break;
		case "NINJA_F":
		case "SWORDMASTER_F":
		case "SWORDMASTER_M":
		case "MASTER_OF_ARMS_F":
		case "MASTER_OF_ARMS_M":
			$headx = [0,0,0,0];
			$heady = [-2,-2,-1,-1];
		break;
		case "NINJA_M":
			$headx = [-2,-2,-2,-2];
			$heady = [-1,-1,0,0];
		break;
		case "SNIPER_F":
		case "SNIPER_M":
			$headx = [0,0,0,0];
			$heady = [-1,-2,-2,-2];
		break;
		case "ARCHER_F":
		case "ARCHER_M":
			$headx = [-1,-1,-1,-1];
			$heady = [-1,-1,-1,0];
		break;
		case "ONMYOJI_F":
		case "ONMYOJI_M":
		case "WOLFSKIN_F":
		case "WOLFSSEGNER_F":
		case "WOLFSSEGNER_M":
			$headx = [0,0,0,0];
			$heady = [-1,-1,-1,-1];
		break;
		case "WOLFSKIN_M":
			$headx = [-1,-1,-1,-1];
			$heady = [1,0,0,-1];
		break;
		case "OUTLAW_F":
			$headx = [1,1,1,1];
			$heady = [-2,-2,-2,-1];
		break;
		case "OUTLAW_M":
			$headx = [-2,-2,-2,-2];
			$heady = [-1,-1,-1,0];
		break;
		case "KNIGHT":
		case "GENERAL":
			$headx = [-4,-4,-4,-4];
			$heady = [-2,-2,-2,-2];
		break;
		case "MASTER_NINJA_F":
		case "VANGUARD":
			$headx = [1,1,1,1];
			$heady = [-2,-2,-1,-1];
		break;
		case "MASTER_NINJA_M":
			$headx = [-2,-2,-2,-2];
			$heady = [-2,-2,-1,-1];
		break;
		case "WITCH":
			$headx = [-1,-1,-1,-1];
			$heady = [-2,-2,-1,-1];
		break;
		case "GREAT_LORD":
		case "LODESTAR":
			$headx = [2,2,2,2];
			$heady = [-2,-2,-1,-1];
		break;
		case "DREAD_FIGHTER_F":
		case "DREAD_FIGHTER_M":
			$headx = [-2,-2,-2,-2];
			$heady = [-2,-2,-1,-1];
		break;
		case "BASARA_F":
		case "BASARA_M":
			$headx = [1,1,2,2];
			$heady = [-2,-3,-3,-3];			
		break;
		case "APOTHECARY_F":
		case "APOTHECARY_M":
			$headx = [-2,-2,-2,-2];
			$heady = [-2,-1,-2,-3];
		break;
		case "MERCHANT_F":
		case "MERCHANT_M":
			$headx = [0,0,0,1];
			$heady = [-3,-2,-1,0];
		break;
		case "SPEAR_FIGHTER_F":
		case "SPEAR_FIGHTER_M":
			$headx = [1,1,1,1];
			$heady = [-1,-1,0,-2];
		break;
		case "SPEAR_MASTER_F":
			$headx = [-1,-1,0,0];
			$heady = [-3,-3,-3,-3];
		break;
		case "SPEAR_MASTER_M":
			$headx = [0,0,0,0];
			$heady = [-3,-3,-3,-3];
		break;
		case "ONI_SAVAGE_F":
		case "ONI_CHIEFTAIN_F":
		case "BLACKSMITH_F":
		case "BLACKSMITH_M":
			$headx = [0,0,0,0];
			$heady = [0,0,-1,-1];
		break;
		case "ONI_CHIEFTAIN_M":
			$headx = [1,1,1,1];
			$heady = [1,0,-1,0];
		break;
		case "ONI_SAVAGE_M":
			$headx = [1,1,1,1];
			$heady = [0,-1,-2,-1];
		break;
		case "KITSUNE_F":
			$headx = [2,2,2,2];
			$heady = [-1,-1,0,0];
		break;
		case "KITSUNE_M":
			$headx = [0,0,0,0];
			$heady = [-2,-2,-1,-1];
		break;
		case "NINE-TAILS_F":
		case "NINE-TAILS_M":
			$headx = [0,0,0,0];
			$heady = [-3,-3,-2,-2];
		break;	
		case "VILLAGER_F":
		case "VILLAGER_M":
			$headx = [1,1,1,1];
			$heady = [-1,0,0,-1];
		break;
		case "NOHR_PRINCE":
		case "NOHR_PRINCESS":
		case "NOHR_NOBLE_F":
		case "NOHR_NOBLE_M":
		case "HOSHIDO_NOBLE_F":
		case "HOSHIDO_NOBLE_M":
			$headx = [-1,-1,-1,-1];
			$heady = [-3,-3,-2,-2];
		break;		
		case "GRANDMASTER":
			$headx = [-1,-1,-1,-1];
			$heady = [-3,-3,-3,-3];
		break;
	}

	$head_back = seperate_layer("characters/".$char.".png","back",$head_size,"head");
	$head_mid = seperate_layer("characters/".$char.".png","mid",$head_size,"head");

	$frames = [];

	// Fill array of animations with data

	for ($i=0;$i<6;$i++) {
		
		$main = imagecreatetruecolor(32, 32);
		imagesavealpha($main, true);
		$transparent = imagecolorallocatealpha($main,0,0,0,127);
		imagefill($main, 0, 0, $transparent);

		// Multiply in-between frames to match 6 frames
		switch ($i) {
			case 0:
				imagecopy($main,$head_back[0],$headx[0],$heady[0],0,0,$head_size,$head_size);
				imagecopy($main,$class_back[0],0,0,0,0,32,32);
				imagecopy($main,$head_mid[0],$headx[0],$heady[0],0,0,$head_size,$head_size);
				imagecopy($main,$class_mid[0],0,0,0,0,32,32);
			break;
			case 1:
			case 5:
				imagecopy($main,$head_back[1],$headx[1],$heady[1],0,0,$head_size,$head_size);
				imagecopy($main,$class_back[1],0,0,0,0,32,32);
				imagecopy($main,$head_mid[1],$headx[1],$heady[1],0,0,$head_size,$head_size);
				imagecopy($main,$class_mid[1],0,0,0,0,32,32);
			break;
			case 2:
			case 4:
				imagecopy($main,$head_back[2],$headx[2],$heady[2],0,0,$head_size,$head_size);
				imagecopy($main,$class_back[2],0,0,0,0,32,32);
				imagecopy($main,$head_mid[2],$headx[2],$heady[2],0,0,$head_size,$head_size);
				imagecopy($main,$class_mid[2],0,0,0,0,32,32);
			break;
			case 3:
				imagecopy($main,$head_back[3],$headx[3],$heady[3],0,0,$head_size,$head_size);
				imagecopy($main,$class_back[3],0,0,0,0,32,32);
				imagecopy($main,$head_mid[3],$headx[3],$heady[3],0,0,$head_size,$head_size);
				imagecopy($main,$class_mid[3],0,0,0,0,32,32);
			break;
		}
		
		$frames[]= $main;
		
	}

	$gc = new GifCreator();
	$gc->create($frames, array(17,17,17,17,17,17), 0);
	$gifBinary = $gc->getGif();
	
	$filename = "output/".$char.'/'.$char.'_'.$class.'.gif';

	file_put_contents($filename, $gifBinary);
	$html_data .= "<tr><td><img src=\"".$filename."\" width=\"32\" height=\"32\"></td><td>".$class."</td></tr>";
}

//}

echo "<table><thead><th>Animation</th><th>Klasse</th></thead><tbody>".$html_data."</tbody></table>";

?>